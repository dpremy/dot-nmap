#!/usr/bin/env bash

# Nmap options are:
#  -sS - TCP SYN scan
#  -v - verbose
#  -T1 - timing of scan. Options are paranoid (0), sneaky (1), polite (2), normal (3), aggressive (4), and insane (5)
#  -sF - FIN scan (can sneak through non-stateful firewalls)
#  -PE - ICMP echo discovery probe
#  -PP - timestamp discovery probe
#  -PY - SCTP init ping
#  -p  - Only scan specified ports
#  -g  - use given number as source port
#  -A  - enable OS detection, version detection, script scanning, and traceroute (aggressive)
#  -O  - enable OS detection
#  -sA - TCP ACK scan
#  -F  - fast scan
#  -oA - Log all output formats to file
#  --open - Only show open ports
#  --script=vulscan - also access vulnerabilities in target

if (`which nmap >/dev/null 2>&1`); then
  # Scan for open ports on targetsrget
    alias nmap_open_ports='nmap --open -oA /tmp/nmap_open_ports_$(date +%y-%m-%d-%H-%M-%S)'

  # List all network interfaces on host where the command runs
    alias nmap_list_interfaces='nmap --iflist -oA /tmp/nmap_list_interfaces_$(date +%y-%m-%d-%H-%M-%S)'

  # Slow scan that avoids to spam the targets logs
    alias nmap_slow='nmap -sS -v -T1 -oA /tmp/nmap_slow_$(date +%y-%m-%d-%H-%M-%S)'

  # Scan to see if hosts are up with TCP FIN scan
    alias nmap_fin='nmap -sF -v -oA /tmp/nmap_fin_$(date +%y-%m-%d-%H-%M-%S)'

  # Aggressive full scan that scans all ports, tries to determine OS and service versions
    alias nmap_full='nmap -sS -T4 -PE -PP -PS80,443 -PY -g 53 -A -p1-65535 -v -oA /tmp/nmap_full_$(date +%y-%m-%d-%H-%M-%S)'

  # TCP ACK scan to check for firewall existence
    alias nmap_check_for_firewall='nmap -sA -p1-65535 -v -T4 -oA /tmp/nmap_check_for_firewall_$(date +%y-%m-%d-%H-%M-%S)'

  # Host discovery with SYN and ACK probes instead of just pings to avoid firewall restrictions
    alias nmap_ping_through_firewall='nmap -PS -PA -oA /tmp/nmap_through_firewall_$(date +%y-%m-%d-%H-%M-%S)'

  # Fast scan of the top 300 popular ports
    alias nmap_fast='nmap -F -T5 --version-light --top-ports 300 -oA /tmp/nmap_fast_$(date +%y-%m-%d-%H-%M-%S)'

  # Detects versions of services and OS, runs on all ports
    alias nmap_detect_versions='nmap -sV -p1-65535 -O --osscan-guess -T4 -Pn -oA /tmp/nmap_detect_versions_$(date +%y-%m-%d-%H-%M-%S)'

  # Uses vulscan script to check target services for vulnerabilities
    alias nmap_check_for_vulns='nmap --script=vulscan -oA /tmp/nmap_check_for_vulns_$(date +%y-%m-%d-%H-%M-%S)'

  # Same as full but via UDP
    alias nmap_full_udp='nmap -sS -sU -T4 -A -v -PE -PS22,25,80 -PA21,23,80,443,3389 -oA /tmp/nmap_full_udp_$(date +%y-%m-%d-%H-%M-%S)'

  # Try to traceroute using the most common ports
    alias nmap_traceroute='nmap -sP -PE -PS22,25,80 -PA21,23,80,3389 -PU -PO --traceroute -oA /tmp/nmap_traceroute_$(date +%y-%m-%d-%H-%M-%S)'

  # Same as nmap_full but also runs all the scripts
    alias nmap_full_with_scripts='nmap -sS -sU -T4 -A -v -PE -PP -PS21,22,23,25,80,113,31339 -PA80,113,443,10042 -PO --script all -oA /tmp/nmap_full_with_scripts_$(date +%y-%m-%d-%H-%M-%S)'

  # Little "safer" scan for OS version as connecting to only HTTP and HTTPS ports doesn't look so attacking.
    alias nmap_web_safe_osscan='nmap -p 80,443 -O -v --osscan-guess --fuzzy -oA /tmp/nmap_web_safe_osscan_$(date +%y-%m-%d-%H-%M-%S)'

  # Uses default and safe scripts to check target for for vulnerabilities without causing unsafe test
    alias nmap_full_with_safe_scripts='nmap -T4 --open -v --script "default and safe" -oA /tmp/nmap_indepth_scan_$(date +%y-%m-%d-%H-%M-%S)'

  # Scan host for open, non-hidden, SMB shares
    alias nmap_open_shares='nmap -p 445 -T4 --open -v --script smb-enum-shares.nse -oA /tmp/nmap_open_shares_$(date +%y-%m-%d-%H-%M-%S)'
fi
